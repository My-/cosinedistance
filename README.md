# CosineDistance

This is my implementation of the String comparison using [Cosine distance](https://en.wikipedia.org/wiki/Cosine_similarity). That was the OOP project. It was well graded and in the feedback I received lecturer called it "Exemplary". The project was done using *"Start-Do-Repeat"* and *"Do more, learn more"* approaches (see below what I meant). 

Extras were done:

- UI was done in `JavaFx` with drag and drop functionality.
- Use of `CompleatableFeatures` and `Thread pools`
- Infinite comparison. The design approach was taken in theory should allow infinite file comparison and they can be added at any time even while the program runs. It was achieved by creating custom `Buffer`, by delegating  `BlockingQueue`, and queueing files in it. Aswell by removing the processed file from custom DB. It ensured what running program does not run out of memory.


Code lives in [submision branch](https://gitlab.com/My-/cosinedistance/tree/submision).



*"Start-Do-Repeat"* approach - is then: you start a project, doing the implementation and if not happy with an outcome you repeat all over from scratch. Obviously reusing parts you are happy with (if there is any). If happy with the outcome - ship it. I came up with this approach (I bet there is already a similar idea somewhere). It is different from Scrum in the way that in each iteration instead of working on an existing project and trying to improve it you scrap it and start from a beginning (the new one). In this way developer are less influenced by whatever it was you weren't happy about the existing version. Using Start-Do-Repeat approach final result comes slower but with higher quality and greater skill outcome for the developer.

*"Do more, learn more"* approach  - is another of those "I invented, but probably someone had said it already". Idea is to do as many extras as possible to a given time. Because by doing more we learn more! We in the college anyway.